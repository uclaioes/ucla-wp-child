<?php
// ==============================================================
// ACF DYNAMIC CONTENT BLOCKS TURNS WORDPRESS IN A REAL CMS
// ==============================================================
// TABLE OF CONTENTS
// 01: INTRODUCTION BLOCK
// 02: SECTION BLOCK
// 03: CARDS BLOCK
// 04: TEXT BLOCK
// 05: IMAGE BLOCK
// 06: CTA BLOCK
// 07: EVENTS BLOCK
// 08: POST BLOCK
// 09: PAGES BLOCK
// 10: RESOURCE BLOCK
// 11: PEOPLE BLOCK
// 12: PROJECTS BLOCK
// 13: VIDEOS AND SOCIAL MEDIA OEMBEDS BLOCK
// 14: STATS BLOCK
// 15: ACCORDIAN BLOCK
// ==============================================================
// END TABLE OF CONTENTS
// ==============================================================
?>

<main id="site-content" role="main">

<?php
// THE MAGIC BEGINS

if( have_rows('ucla-c-content-blocks') ):
  ?>

<div class="ucla-acf-components-wrapper">
  <?php

	 // loop through the rows of data
		while ( have_rows('ucla-c-content-blocks') ) : the_row();

			// ==============================================================
      // 01. START INTRO BLOCK
      // ==============================================================
        
        if( get_row_layout() == 'ucla-c-intro-block' ):

          $ucla_c_intro_heading = get_sub_field('ucla-c-intro-heading');
          $ucla_c_intro_standfirst = get_sub_field('ucla-c-intro-standfirst');
          $ucla_c_intro_lede = get_sub_field('ucla-c-intro-lede');
          $ucla_c_intro_desc = get_sub_field('ucla-c-intro-desc');
        
          ?>
    <div class="ucla-c-intro-block">
    <?php if( !empty( $ucla_c_intro_heading ) ): ?>
      <h2><?php echo $ucla_c_intro_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_intro_standfirst ) ): ?>
      <p class="standfirst"><?php echo $ucla_c_intro_standfirst; ?></p>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_intro_lede ) ): ?>
      <p class="lede"><?php echo $ucla_c_intro_lede; ?></p>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_intro_desc ) ): ?>
      <?php echo $ucla_c_intro_desc; ?>
    <?php endif; ?> 
    </div>
  <?php
        endif;	
        // ==============================================================
        // 01. END INTRO BLOCK
        // ==============================================================

      ?>




  <?php							
  // ==============================================================
  // 02. START SECTION BLOCK 
  // ==============================================================

  if( get_row_layout() == 'ucla-c-section-block' ):
    
    echo 	'<div class="ucla-c-section">';
    
  
    $ucla_c_section_block_heading = esc_html(get_sub_field('ucla-c-section-block-heading'));
    $ucla_c_section_block_title = esc_html(get_sub_field('ucla-c-section-block-title'));
    $ucla_c_section_block_image = get_sub_field('ucla-c-section-block-image');
    $ucla_c_section_block_desc = get_sub_field('ucla-c-section-block-desc');
    $ucla_c_section_block_links = esc_html(get_sub_field('ucla-c-section-block-links'));
    $size = 'full'; 
  

    if( !empty ($ucla_c_section_block_heading)):
      echo '<h2>'. $ucla_c_section_block_heading .'</h2>';
    endif;  

    if (!empty ($ucla_c_section_block_image) ): 
      echo wp_get_attachment_image( $ucla_c_section_block_image, $size );
      ?>
      <img src="<?php echo esc_url($ucla_c_section_block_image['url']); ?>"
    alt="<?php echo esc_attr($ucla_c_section_block_image['alt']); ?>" />
  <?php endif; 
      
  
  if( !empty ($ucla_c_section_block_title)):
    echo '<h3>'. $ucla_c_section_block_title .'</h3>';
  endif;  
  if( !empty ($ucla_c_section_block_desc)):
    echo '<div class="ucla-c-section-desc">';
    echo  $ucla_c_section_block_desc;
    echo '</div>';
  endif;
  // links repeater starts here 
  if( have_rows('ucla-c-section-block-links') ): // check if the nested repeater field has rows of data
    echo '<ul>';
    while (have_rows('ucla-c-section-links')): the_row();
    
    
      $ucla_c_section_block_link_title = get_sub_field('ucla-c-section-block-link-title');
      $ucla_c_section_block_link_url = get_sub_field('ucla-c-section-block-link-url'); 
    if ($ucla_c_section_block_links): ?>
  <li><a
      href="<?php echo esc_url($ucla_c_section_block_link_url); ?>"><?php echo $ucla_c_section_block_link_title; ?></a>
  </li>
  <?php endif;

  endwhile; 
  echo "</ul>";	
endif; // links repeater ends here 

echo '</div>';
endif; 

// ==============================================================
// 02. END SECTION BLOCK
// ==============================================================

?>





  <?php 
// ==============================================================
// 03. START CARD BLOCK
// ==============================================================
//wp_reset_query();
      if( get_row_layout() == 'ucla-c-card-block' ): // check current row layout
        $ucla_c_cards_heading = get_sub_field('ucla-c-cards-heading');
        $ucla_c_cards_summary = get_sub_field('ucla-c-cards-summary');
  
        
				if( have_rows('ucla-c-card-list') ): // check if the nested repeater field has rows of data
         
			?>
   <div class="ucla-c-cards">
    <?php
if( !empty( $ucla_c_cards_heading ) ): ?>
    <h2><?php echo $ucla_c_cards_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_cards_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_cards_summary; ?></p>
    <?php endif; ?>
    <div class="ucla-c-card-stack">
    <?php
      while (have_rows('ucla-c-card-list')): the_row();
        

				// vars
				$ucla_c_card_heading = get_sub_field('ucla-c-card-heading');
				$ucla_c_card_desc = get_sub_field('ucla-c-card-desc');
        $ucla_c_card_image = get_sub_field('ucla-c-card-image');
        $size = 'medium'; 
				$ucla_c_card_link = get_sub_field('ucla-c-card-link');
				//$ucla_c_card_link_title = get_sub_field('ucla-c-card-link-title');
				?>
    <article class="ucla-c-card h-entry">

      <?php if (!empty ($ucla_c_card_image) ): ?>
      <?php if ($ucla_c_card_link): ?><a class="u-url"
        href="<?php echo $ucla_c_card_link; ?>"><?php endif; ?>
        <?php
          echo wp_get_attachment_image( $ucla_c_card_image, $size );
        ?>
      <?php endif; ?>
      <?php if ( !empty ($ucla_c_card_heading) ):		?>
      <h2 class="p-name"><?php echo $ucla_c_card_heading; ?></h2>
      <?php endif; ?>
      <?php if ($ucla_c_card_link): ?></a><?php endif; ?>
      <?php if( !empty ($ucla_c_card_desc)):		?>
        <?php echo $ucla_c_card_desc; ?>
      <?php endif; ?>
    </article>

    <?php endwhile; ?>
    </div>
  </div>
  <?php endif; ?>
  <?php endif; 
// ==============================================================
// 03. CLOSE CARD BLOCK 
// ==============================================================
?>





<?php
// ==============================================================
// 04. START TEXT BLOCK 
// ==============================================================

if( get_row_layout() == 'ucla-c-text-block' ):
  $ucla_c_text_heading = get_sub_field('ucla-c-text-heading');
  $ucla_c_text = get_sub_field('ucla-c-text');

  ?>
  <div class="ucla-c-text">
    <?php if ($ucla_c_text_heading): ?>
    <h2><?php echo $ucla_c_text_heading; ?></h2>
    <?php endif;	?>
    <?php echo $ucla_c_text; ?>
  </div>
  <?php
endif;	
// ==============================================================
//04. END TEXT BLOCK
// ==============================================================

?>



  <?php
// ==============================================================
// 05. START IMAGE BLOCK
// ==============================================================
wp_reset_query();
if( get_row_layout() == 'ucla-c-image-block' ):

  $ucla_c_image = get_sub_field('ucla-c-image');
  $ucla_c_image_link = get_sub_field('ucla-c-image-link');

  ?>
  <div class="ucla-c-image-block">
    <?php if ($ucla_c_image_link): ?>
    <a class="ucla-c-img-link" href="<?php echo $ucla_c_image_link; ?>">
      <?php endif; ?>
      <img src="<?php echo $ucla_c_image['url']; ?>"
        alt="<?php echo $ucla_c_image['alt'] ?>" />
      <?php if ($ucla_c_image_link): ?>
    </a>
    <?php endif; ?>
  </div>
  <?php
endif;	
// ==============================================================
// 05. CLOSE IMAGE BLOCK
// ==============================================================

?>


  <?php
// ==============================================================  
// 06. START CTA BLOCK
// ==============================================================
wp_reset_query();        
if( get_row_layout() == 'ucla-c-cta-block' ):

  $ucla_c_cta_text = get_sub_field('ucla-c-cta-text');
  $ucla_c_cta_link = get_sub_field('ucla-c-cta-link');
  ?>
  <div class="ucla-c-cta">
    <button><a 
      href="<?php echo $ucla_c_cta_link; ?>"><?php echo $ucla_c_cta_text; ?></a></button>
  </div>

  <?php
endif;
// ==============================================================
// 06. END CTA BLOCK
// ==============================================================
        
?>



  <?php
// ==============================================================
// 07. START EVENT BLOCK
// ==============================================================
?>

  <?php 
  wp_reset_query();
  if( get_row_layout() == 'ucla-c-events-block' ): 
    $ucla_c_events_heading = get_sub_field('ucla-c-events-heading');
    $ucla_c_events_summary = get_sub_field('ucla-c-events-summary');

  $posts = get_sub_field('ucla-c-events-list');
  if( $posts ): ?>
  <div class="ucla-c-events">
    <?php
if( !empty( $ucla_c_events_heading ) ): ?>
    <h2><?php echo $ucla_c_events_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_events_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_events_summary; ?></p>
    <?php endif; ?>
    <div class="h-feed">
    <?php foreach ( $posts as $post):  ?>
    <article class="h-entry" id="post-<?php the_ID(); ?>">

      <figure>
        
          <?php setup_postdata($post); ?>
          <?php the_post_thumbnail( 'medium', ['class' => 'ucla-c-event-img'] ); ?>

      </figure>
      <h2 class="p-name"><a href="<?php the_permalink(); ?>"
          rel="bookmark"><?php the_title(); ?></a></h2>

      <?php
      if( get_field('ucla-e-alt-name') ): ?>
      <p class="p-summary">
        <?php echo esc_html( get_field('ucla-e-alt-name') ); ?></p>
      <?php endif; ?>

      <div class="e-content">
        <?php
    $event_start = get_field('ucla-e-start-date'); 
    if ( $event_start ) : ?>
        <time class="event-date"
          datetime="<?php echo ucla_html_date( $event_start ) ; ?>"><?php echo ucla_public_date( $event_start ); ?></time>
        <time class="event-time-start"
          datetime="<?php echo ucla_html_time( $event_start ) ; ?>"><?php echo ucla_public_time( $event_start ); ?></time>
        <?php endif; ?>
        <?php
      $event_end_time = get_field('ucla-e-end-time'); 
			if ( $event_end_time ) : ?>
        - <time class="event-time-end"
          datetime="<?php echo ucla_html_time( $event_end_time ); ?>"><?php echo ucla_public_time($event_end_time); ?></time>
        <hr>
        <?php endif; ?>
      </div>
    </article>
    <?php endforeach; ?>
    </div>
  </div>
  <?php 
  endif; 
endif; 
wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

  <?php 
// ==============================================================
// 07: END EVENT BLOCK 
// ==============================================================

?>



  <?php 

// ==============================================================
// 08. START POST BLOCK
// ==============================================================
wp_reset_query();
if( get_row_layout() == 'ucla-c-blog-posts-block' ): 
  $ucla_c_blog_posts_heading = get_sub_field('ucla-c-blog-posts-heading');
  $ucla_c_blog_posts_summary = get_sub_field('ucla-c-blog-posts-summary');

$posts = get_sub_field('ucla-c-post-list');
if( $posts ): ?>
  <div class="ucla-c-blog-posts">
    <?php
if( !empty( $ucla_c_blog_posts_heading ) ): ?>
    <h2><?php echo $ucla_c_blog_posts_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_blog_posts_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_blog_posts_summary; ?></p>
    <?php endif; ?>
    <div class="h-feed">
    <?php foreach ( $posts as $post):  ?>
    <article class="h-entry" id="post-<?php the_ID(); ?>">
      <figure>
        <a href="<?php the_permalink(); ?>" rel="bookmark">
          <?php setup_postdata($post); ?>
          <?php the_post_thumbnail( 'medium', ['class' => 'ucla-c-post-img'] ); ?>
        </a>
      </figure>
      <div class="e-content">

        <time class="dt-published"
          datetime="<?php echo get_post_time( 'Y-n-j' ); ?>"><?php echo get_post_time( 'F j, Y' ); ?></time>
        <h2 class="p-name"><a href="<?php the_permalink(); ?>"
            rel="bookmark"><?php the_title(); ?></a></h2>
        <div class="lede p-summary"><?php the_excerpt(); ?></div>
      </div>
    </article>

    <?php endforeach; ?>
    </div>
    </div>
  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

  <?php endif; 
endif;
?>
  <?php	
// ==============================================================
// 08. END POST BLOCK 
// ==============================================================
?>




  <?php
// ==============================================================
// 09. START PAGES BLOCK
// ==============================================================
?>

  <?php
  wp_reset_query();
  if( get_row_layout() == 'ucla-c-pages-block' ): 
    $ucla_c_pages_heading = get_sub_field('ucla-c-pages-heading');
    $ucla_c_pages_summary = get_sub_field('ucla-c-pages-summary');

		$posts = get_sub_field('ucla-c-page-list');
    if( $posts ): 
?>
  <div class="ucla-c-pages">
    <?php
if( !empty( $ucla_c_pages_heading ) ): ?>
    <h2><?php echo $ucla_c_pages_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_pages_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_pages_summary; ?></p>
    <?php endif; ?>
    <div class="container">
    <?php foreach ( $posts as $post):  ?>
    <article class="h-entry" id="post-<?php the_ID(); ?>">

      <?php setup_postdata($post); 
        if ( has_post_thumbnail() ):
          ?>

      <?php the_post_thumbnail( 'thumb', ['class' => 'ucla-c-pages-block-img'] ); ?>

      <?php endif; ?>

      <p class="p-name"><a
          href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>



    </article>

    <?php endforeach; ?>
    </div>
  </div>

  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

  <?php 
  endif;
endif;
// ==============================================================
// 09. END PAGES BLOCK
// ==============================================================

?>



  <?php
// ==============================================================
// 10. START RESOURCES BLOCK
// ==============================================================
?>

  <?php
  wp_reset_query();
  if( get_row_layout() == 'ucla-c-resource-block' ):
    $ucla_c_resources_heading = get_sub_field('ucla-c-resources-heading');
    $ucla_c_resources_summary = get_sub_field('ucla-c-resources-summary');

		$posts = get_sub_field('ucla-c-resource-list');
    if( $posts ): 
?>
  <div class="ucla-c-resources">
    <?php
if( !empty( $ucla_c_resources_heading ) ): ?>
    <h2><?php echo $ucla_c_resources_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_resources_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_resources_summary; ?></p>
    <?php endif; ?>
    <div class="container">
    <ul>
      <?php foreach ( $posts as $post):  ?>
      <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
      <?php endforeach; ?>
    </ul>
    </div>
  </div>

  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

  <?php 
  endif;
endif;  
// ==============================================================
// 10. END RESOURCES BLOCK
// ==============================================================

?>



  <?php
// ==============================================================
// 11. START PEOPLE BLOCK
// ==============================================================
?>


  <?php
	wp_reset_query();
  if( get_row_layout() == 'ucla-c-people-block' ):
    $ucla_c_people_heading = get_sub_field('ucla-c-people-heading');
    $ucla_c_people_summary = get_sub_field('ucla-c-people-summary');

		$posts = get_sub_field('ucla-c-people-list');
    if( $posts ): 
?>
  <div class="ucla-c-people">
    <?php
if( !empty( $ucla_c_people_heading ) ): ?>
    <h2><?php echo $ucla_c_people_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_people_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_people_summary; ?></p>
    <?php endif; ?>
    <div class="h-feed">

    <?php foreach ( $posts as $post):  
    $ucla_p_job_title = get_field('ucla-p-job-title');
    $ucla_p_org = get_field( 'ucla-p-org' );
    $image = get_field( 'ucla-p-photo' );
	
    if( $image ):
      // Image variables.
      $url = $image['url'];
      $title = $image['title'];
      $alt = $image['alt'];
      $caption = $image['caption'];
      $size = 'square_lrg';
			$avatar = $image['sizes'][ $size ];
    endif;
		 ?>

    <article class="h-entry" id="post-<?php the_ID(); ?>">
      <?php if ( $image ): ?>
      <figure>
        <a href="<?php the_permalink(); ?>">
          <img class="avatar u-photo" src="<?php echo esc_url($avatar); ?>"
            alt="<?php echo esc_attr($alt); ?>" />
        </a>
      </figure>
      <?php endif; ?>
      <p class="p-name"><a
          href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
        <?php if( $ucla_p_job_title ): ?>
				  <p class="p-job-title"><?php echo esc_html( $ucla_p_job_title ); ?><?php if( $ucla_p_org ): ?>, <span class="p-org"><?php echo esc_html( $ucla_p_org ); ?></span>
				  <?php endif; ?>
        </p>
			<?php endif; ?>
				
    </article>

    <?php endforeach; ?>
  </div></div>

  <?php wp_reset_postdata();  ?>

  <?php 
  endif;
endif;  
// ==============================================================
// 11. END PEOPLE BLOCK
// ==============================================================
?>




  <?php
// ==============================================================
// 12. START PROJECTS BLOCK
// ==============================================================
?>

  <?php
  wp_reset_query();
  if( get_row_layout() == 'ucla-c-project-block' ): 
    $ucla_c_project_heading = get_sub_field('ucla-c-project-heading');
    $ucla_c_project_summary = get_sub_field('ucla-c-project-summary');
  
		$posts = get_sub_field('ucla-c-project-list');
    if( $posts ): 
?>
  <div class="ucla-c-projects">
    <?php
if( !empty( $ucla_c_project_heading ) ): ?>
    <h2><?php echo $ucla_c_project_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_project_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_project_summary; ?></p>
    <?php endif; ?>
      <div class="h-feed">
    <?php foreach ( $posts as $post):  
    $ucla_project_summary = get_field('ucla-project-summary');
    ?>
    <article class="h-entry" id="post-<?php the_ID(); ?>">
      <?php if (!empty ( the_post_thumbnail() ) ): ?>
      <figure>
        <a href="<?php the_permalink(); ?>">
          <?php setup_postdata($post); ?>
          <?php the_post_thumbnail( 'medium', ['class' => 'ucla-c-project-img'] ); ?>
        </a>
      </figure>
      <?php endif; ?>
      <h2 class="p-name"><a
          href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <?php if( !empty( $ucla_project_summary ) ): ?>
      <p class="lede p-summary"><?php echo $ucla_project_summary ?></p>
      <?php endif; ?>


    </article>

    <?php endforeach; ?>
  </div></div>

  <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

  <?php 
  endif;
endif;
// ==============================================================
// 12. END PROJECTS BLOCK
// ==============================================================

?>


  <?php	
// ==============================================================
// 13. START OEMBED MEDIA BLOCK
// https://wordpress.org/support/article/embeds/
// ==============================================================

if( get_row_layout() == 'ucla-c-oembed-block' ):

			$ucla_c_oembed_heading = get_sub_field('ucla-c-oembed-heading');
			$ucla_c_oembed_summary = get_sub_field('ucla-c-oembed-summary');
      $ucla_c_oembed_url = get_sub_field('ucla-c-oembed-url');
      $ucla_c_embed_posts = get_sub_field('ucla-c-oembed-list');
       
		
			?>
  <div class="ucla-c-oembed">

    <?php if( !empty( $ucla_c_oembed_heading ) ): ?>
    <h2><?php echo $ucla_c_oembed_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_oembed_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_oembed_summary; ?></p>
    <?php endif; ?>
    <?php echo $ucla_c_oembed_url; ?>

    
    <?php
				// Check if ACF repeater rows exists for embedded media.
				if( have_rows('ucla-c-oembed-list') ):
				?>
				
				<div class="ucla-c-oembeds">
					<?php 	
					// Loop through rows
					while( have_rows('ucla-c-oembed-list') ) : the_row(); ?>
						<figure>
							<?php if( get_sub_field('ucla-c-oembed-name') ):?>
							<figcaption><?php the_sub_field('ucla-c-oembed-name'); ?></figcaption>
							<?php endif; ?>
            <?php if( get_sub_field('ucla-c-oembed-url') ):?>
						  <?php the_sub_field('ucla-c-oembed-url'); ?>
            <?php endif; ?>
              
					</figure>
					<?php // End loop for embeds.
						endwhile; ?>
				</div>

				<?php endif; // end repeater for embed ?>	
  </div>
  <?php
endif;	
// ==============================================================
// 13. END OEMBED MEDIA BLOCK
// ==============================================================

?>




  <?php  

// ==============================================================
// 14. START STATS BLOCK
// ==============================================================


			if( get_row_layout() == 'ucla-c-stats-block' ): // check current row layout
        $ucla_c_stats_block_heading = get_sub_field('ucla-c-stats-block-heading');
        $ucla_c_stats_block_summary = get_sub_field('ucla-c-stats-block-summary');

				if( have_rows('ucla-c-stats-block-list') ): // check if the nested repeater field has rows of data

			?>
  <div class="ucla-c-stats">
    <?php
if( !empty( $ucla_c_stats_block_heading ) ): ?>
    <h2><?php echo $ucla_c_stats_block_heading; ?></h2>
    <?php endif; ?>
    <?php if( !empty( $ucla_c_stats_block_summary ) ): ?>
    <p class="lede"><?php echo $ucla_c_stats_block_summary; ?></p>
    <?php endif; ?>
    <div class="container">
    <?php
					
			while (have_rows('ucla-c-stats-block-list')): the_row();
				// vars
				$ucla_c_stats_block_value = get_sub_field('ucla-c-stats-block-value');
				$ucla_c_stats_block_metric = get_sub_field('ucla-c-stats-block-metric');
				$ucla_c_stats_block_desc = get_sub_field('ucla-c-stats-block-desc');
				$ucla_c_stats_block_link = get_sub_field('ucla-c-stats-block-link');
			
				?>
    <figure class="ucla-c-stat">

      <?php if ( !empty ($ucla_c_stats_block_value) ):		?>
      <span class="ucla-c-stat-value">
        <?php echo $ucla_c_stats_block_value; ?>
      </span>
      <?php endif; ?>

      <?php if( !empty ($ucla_c_stats_block_metric)):		?>
      <figcaption class="ucla-c-stat-metric">
        <?php echo $ucla_c_stats_block_metric; ?>

        <?php endif; ?>

        <?php if( !empty ($ucla_c_stats_block_desc)):		?>
        <span class="ucla-c-stat-desc">
          <?php echo $ucla_c_stats_block_desc; ?>
        </span>
        <?php endif; ?>
        <?php if ($ucla_c_stats_block_link): ?>
        <a class="u-url" href="<?php echo $ucla_c_stats_block_link; ?>"><svg
            class="ucla-icon arrow-forward" width="48" height="24"
            viewBox="0 0 48 24" aria-labelledby="icon-arrow-forward" role="img">
            <title id="icon-arrow-forward">External Link</title>
            <g>
              <path id="Path"
                d="M36 4l-1.41 1.41L40.17 11H4v2h36.17l-5.58 5.59L36 20l8-8z" />
            </g>
          </svg></a>
        <?php endif; ?>
      </figcaption>
    </figure>

    <?php endwhile; ?>
  </div></div>
  <?php endif; ?>
  <?php endif; 
// ==============================================================
// 14. END STATS BLOCK
// ==============================================================
?>


<?php 
// ==============================================================
// 15. START ACCORDIAN BLOCK
// ==============================================================
//wp_reset_query();
      if( get_row_layout() == 'ucla-c-accordian-block' ): // check current row layout
        
				if( have_rows('ucla-c-accordian-list') ): // check if the nested repeater field has rows of data
         
			?>
  

    <?php
      // create unique counter to add unique id for each item on accordian
      $counter = 1;
      while (have_rows('ucla-c-accordian-list')): the_row();
				// vars
				$ucla_c_accordian_heading = get_sub_field('ucla-c-accordian-heading');
				$ucla_c_accordian_desc = get_sub_field('ucla-c-accordian-desc');
				?>
      <div class="wp-block-pb-accordion-item wp-block-pb-accordion-item c-accordion__item js-accordion-item is-read ucla-c-accordian" data-initially-open="false" data-click-to-close="true" data-auto-close="true" data-scroll="false" data-scroll-offset="0">
      <?php if ( !empty ($ucla_c_accordian_heading) ):		?>
      <h2 id="at-<?php echo $counter; ?>" class="c-accordion__title js-accordion-controller" role="button"><?php echo $ucla_c_accordian_heading; ?></h2>
      <?php endif; ?>

      <?php if( !empty ($ucla_c_accordian_desc)):		?>
      <div id="ac-<?php echo $counter; ?>" class="c-accordion__content">
        <?php echo $ucla_c_accordian_desc; ?>
      </div>
      <?php endif; ?>
      </div>
    <?php $counter++; ?>

    <?php endwhile; ?>


  <?php endif; 
  endif; 
// ==============================================================
// 15. CLOSE ACCORDIAN BLOCK 
// ==============================================================


// ==============================================================
// ADD NEW BLOCKS STARTING HERE

?>





  <?php endwhile;
// CLOSE LOOP OF FLEXIBLE CONTENT
?>


  <?php 

else :
	echo '';
  ?>

</div>

<?php
endif; // END OF ACF BLOCKS 
// THE MAGIC ENDS.
?>
