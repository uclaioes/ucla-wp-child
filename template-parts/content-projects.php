<?php
/**
 * Custom template for displaying projects
 *
 * Used for index or list of projects.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

<main id="site-content" role="main">

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
<?php
		get_template_part( 'template-parts/featured-image' );

	  get_template_part( 'template-parts/entry-header' );

  ?>


	</div><!-- .post-inner -->


		<?php
		
		//edit_post_link();

		// Single bottom post meta.
		twentytwenty_the_post_meta( get_the_ID(), 'single-bottom' );

		if ( post_type_supports( get_post_type( get_the_ID() ), 'author' ) && is_single() ) {

			get_template_part( 'template-parts/entry-author-bio' );

		}
		?>

		<div class="entry-content">
			<?php if( get_field('ucla-project-alt-name') ): ?>
			<p class="standfirst"><?php echo esc_html( get_field('ucla-project-alt-name') ); ?></p>
			<?php endif; ?>


			<?php if( get_field('ucla-project-summary') ): ?>
			<p class="p-summary"><?php echo esc_html( get_field( 'ucla-project-summary' ) ); ?></p>
			<?php endif; ?>

			<?php if( get_field('ucla-project-status') ): ?>
			<p><b>Status</b>: <?php echo esc_html( get_field( 'ucla-project-status' ) ); ?></p>
			<?php endif; ?>
		</div>
		

</article>
