<?php
/**
 * Custom template for displaying people
 *
 * Used for both singular and index.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>

<main id="site-content" role="main">

<?php

$posts = get_posts( array(
	'posts_per_page' => -1,
	'post_type'      => 'event',
	'post_status' => 'publish,future',
	'order'          => 'DESC',
	'orderby'        => 'meta_value',
	'meta_value'   => date( "Ymd" ),
	'meta_compare' => '>=',
	'meta_key'       => 'ucla-e-start-date',
	'meta_type'      => 'DATE',
	'ignore_sticky_posts' => true,
));


if( $posts ) {
	?>
		<h2>Upcoming Events</h2>
		<div class="h-feed">
		<?php	
    foreach( $posts as $post ) {
		?>
		<article <?php post_class("vevent"); ?> id="post-<?php the_ID(); ?>">
		<?php			 
			get_template_part( 'template-parts/featured-image' );
			get_template_part( 'template-parts/entry-header' );
			if( get_field('ucla-e-alt-name') ): ?>
				<p class="standfirst"><?php echo esc_html( get_field('ucla-e-alt-name') ); ?></p>
			<?php endif; ?>
			
			<?php
			$event_start = get_field('ucla-e-start-date'); 
			if ( $event_start ) : ?> 
			<time class="event-date" datetime="<?php echo ucla_html_date( $event_start ) ; ?>"><?php echo ucla_public_date( $event_start ); ?></time>
			<time class="event-time-start" datetime="<?php echo ucla_html_time( $event_start ) ; ?>"><?php echo ucla_public_time( $event_start ); ?></time>
			<?php endif; ?>
	
			<?php 
			$event_end_time = get_field('ucla-e-end-time'); 
			if ( $event_end_time ) : ?> 
				- <time class="event-time-end" datetime="<?php echo ucla_html_time( $event_end_time ); ?>"><?php echo ucla_public_time($event_end_time); ?></time>
				<hr>
			<?php endif; 
		?>
		</article>
		<?php

		}
		?>
		</div>
		<?php
		

}



$posts = get_posts( array(
	'posts_per_page' => -1,
	'post_type'      => 'event',
	'post_status' => 'publish,future',
	'order'          => 'DESC',
	'orderby'        => 'meta_value',
	'meta_value'   => date( "Ymd" ),
	'meta_compare' => '<',
	'meta_key'       => 'ucla-e-start-date',
	'meta_type'      => 'DATE',
	'ignore_sticky_posts' => true,
));


if( $posts ) {
	?>
		<h2>Past Events</h2>
		<div class="h-feed">
		<?php	
    foreach( $posts as $post ) {
		?>
		<article <?php post_class("vevent"); ?> id="post-<?php the_ID(); ?>">
		<?php			 
			get_template_part( 'template-parts/featured-image' );
			get_template_part( 'template-parts/entry-header' );
			if( get_field('ucla-e-alt-name') ): ?>
				<p class="standfirst"><?php echo esc_html( get_field('ucla-e-alt-name') ); ?></p>
			<?php endif; ?>
			
			<?php
			$event_start = get_field('ucla-e-start-date'); 
			if ( $event_start ) : ?> 
			<time class="event-date" datetime="<?php echo ucla_html_date( $event_start ) ; ?>"><?php echo ucla_public_date( $event_start ); ?></time>
			<time class="event-time-start" datetime="<?php echo ucla_html_time( $event_start ) ; ?>"><?php echo ucla_public_time( $event_start ); ?></time>
			<?php endif; ?>
	
			<?php 
			$event_end_time = get_field('ucla-e-end-time'); 
			if ( $event_end_time ) : ?> 
				- <time class="event-time-end" datetime="<?php echo ucla_html_time( $event_end_time ); ?>"><?php echo ucla_public_time($event_end_time); ?></time>
				<hr>
			<?php endif; 
		?>
		</article>
		<?php

		}
		?>
		</div>
		<?php

}
?>

</main>	