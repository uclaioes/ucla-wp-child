<?php
/**
 * Custom template to display a project
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */

?>
<main id="site-content" role="main">

<article <?php post_class("vevent"); ?> id="post-<?php the_ID(); ?>">

<?php
    get_template_part( 'template-parts/featured-image' );
    get_template_part( 'template-parts/entry-header' );

 

	if ( is_single() ) {
		?>
    <?php if( get_field('ucla-e-alt-name') ): ?>
	    <p class="standfirst"><?php echo esc_html( get_field('ucla-e-alt-name') ); ?></p>
    <?php endif; ?>

        <?php 
        $event_start = get_field('ucla-e-start-date'); 
  
        if ( $event_start ) : ?> 
				<time class="event-date" datetime="<?php echo ucla_html_date( $event_start ) ; ?>"><?php echo ucla_public_date( $event_start ); ?></time>
        <time class="event-time-start" datetime="<?php echo ucla_html_time( $event_start ) ; ?>"><?php echo ucla_public_time( $event_start ); ?></time>
				<?php endif; ?>
        
        <?php 
        $event_end_time = get_field('ucla-e-end-time'); 
       
        if ( $event_end_time ) : ?> 
          - <time class="event-time-end" datetime="<?php echo ucla_html_time( $event_end_time ); ?>"><?php echo ucla_public_time($event_end_time); ?></time>
				<?php endif; ?>
        <?php if( get_field('ucla-e-venue') ): ?>
    <p class="event-venue"><?php echo esc_html( get_field( 'ucla-e-venue' ) ); ?></p>
  <?php endif; ?>
  <?php if( get_field('ucla-e-location') ): ?>
    <address class="event-location"><?php echo get_field( 'ucla-e-location' ); ?></address>
  <?php endif; ?>
        
        <?php 
				// display project website
        $rsvp_link = get_field('ucla-e-rsvp-link'); 
        $rsvp_text = get_field('ucla-e-rsvp-text'); 
				if( $rsvp_link ): ?>
					<p><a class="button" href="<?php echo esc_url( $rsvp_link ); ?>"><?php echo esc_html( $rsvp_text ); ?></a></p>
				<?php endif; ?>
        

  <?php if( get_field('ucla-e-summary') ): ?>
    <p class="p-summary lede"><?php echo esc_html( get_field( 'ucla-e-summary' ) ); ?></p>
  <?php endif; ?>



    <div class="e-content">
       <?php the_content(); ?>
			
			
		
        

				<?php
				// Check if ACF repeater rows exists for embedded media.
				if( have_rows('ucla-e-embeds') ):
				?>
				
				<div class="ucla-c-oembeds">
					<?php 	
					// Loop through rows
					while( have_rows('ucla-e-embeds') ) : the_row(); ?>
						<figure>
							<?php if( get_sub_field('ucla-e-embed-name') ):?>
							<figcaption><?php the_sub_field('ucla-e-embed-name'); ?></figcaption>
							<?php endif; ?>
						<?php the_sub_field('ucla-e-embed-url'); ?>
					</figure>
					<?php // End loop for embeds.
						endwhile; ?>
				</div>

				<?php endif; // end repeater for embed ?>	

						
				<?php 
				$images = get_field('ucla-e-gallery');
				$size = 'full'; // (thumbnail, medium, large, full or custom size)
				if( $images ): ?>
				<figure class="ucla-c-gallery">
				<?php foreach( $images as $image_id ): ?>
						
								<?php echo wp_get_attachment_image( $image_id, $size ); ?>
						
				<?php endforeach; ?>
				</figure>
				<?php endif; ?>

         

       

				<?php
				// Check if ACF repeater rows exists for funders.
				if( have_rows('ucla-e-sponsors') ):
				?>
				<div class="ucla-c-sponsors">
				<h2>Sponsors</h2>
				<ul>
					<?php 	
					// Loop through rows
					while( have_rows('ucla-e-sponsors') ) : the_row(); ?>
					<li>
						<?php 
							
								$ucla_event_sponsor_name = get_sub_field('ucla-e-sponsor-name'); 
								$ucla_event_sponsor_description = get_sub_field('ucla-e-sponsor-description'); 
								$ucla_event_sponsor_logo = get_sub_field('ucla-e-sponsor-logo'); 
								$ucla_event_sponsor_website = get_sub_field('ucla-e-sponsor-website'); 
								

								if( $ucla_event_sponsor_name &&  $ucla_event_sponsor_website ): 
									echo "<h3 class=\"p-name\"><a href=" . esc_url( $ucla_event_sponsor_website ) . ">" . $ucla_event_sponsor_name . "</a></h3>"; 
								elseif	( $ucla_event_sponsor_name):
									echo "<h3 class=\"p-name\">" . $ucla_event_sponsor_name . "</h3>"; 
								endif; 

								if( $ucla_event_sponsor_description ): 
									echo "<p class=\"p-summary\">" . $ucla_event_sponsor_description . "</p>"; 
								endif;

								if( $ucla_event_sponsor_logo  &&  $ucla_event_sponsor_website ): ?>
									<a href="<?php echo esc_url( $ucla_event_sponsor_website ); ?>">
									<?php	echo wp_get_attachment_image( $ucla_event_sponsor_logo, 'thumb' ); ?>
									</a>
								<?php	
								elseif ( $ucla_event_sponsor_logo ):
									echo wp_get_attachment_image( $ucla_event_sponsor_logo, 'thumb' ); 
								endif;
							?>	
					</li>
					<?php // End loop for sponsors.
						endwhile; ?>
				</ul>
				</div>		
				<?php endif; // end repeater for sponsors ?>



				<?php
				// Check if ACF repeater rows exists for partners.
				if( have_rows('ucla-e-partners') ):
				?>
				<div class="ucla-c-sponsors">
				<h2>Partners</h2>
				<ul>
					<?php 	
					// Loop through rows
					while( have_rows('ucla-e-partners') ) : the_row(); ?>
					<li>
						<?php 
								
								$ucla_event_partner_name = get_sub_field('ucla-e-partner-name'); 
								$ucla_event_partner_description = get_sub_field('ucla-e-partner-description'); 
								$ucla_event_partner_logo = get_sub_field('ucla-e-partner-logo'); 
								$ucla_event_partner_website = get_sub_field('ucla-e-partner-website'); 
								

								if( $ucla_event_partner_name &&  $ucla_event_partner_website ): 
									echo "<h3 class=\"p-name\"><a href=" . esc_url( $ucla_event_partner_website ) . ">" . $ucla_event_partner_name . "</a></h3>"; 
								elseif	( $ucla_event_partner_name):
									echo "<h3 class=\"p-name\">" . $ucla_event_partner_name . "</h3>"; 
								endif; 

								if( $ucla_event_partner_description ): 
									echo "<p class=\"p-summary\">" . $ucla_event_partner_description . "</p>"; 
								endif;
								if( $ucla_event_partner_logo  &&  $ucla_event_partner_website ): ?>
									<a href="<?php echo esc_url( $ucla_event_partner_website ); ?>">
									<?php	echo wp_get_attachment_image( $ucla_event_partner_logo, 'thumb' ); ?>
									</a>
								<?php	
								elseif ( $ucla_event_partner_logo ):
									echo wp_get_attachment_image( $ucla_event_partner_logo, 'thumb' ); 
								endif;
							?>	
					</li>
					<?php // End loop for partners.
						endwhile; ?>
				</ul>
						</div>
				<?php endif; // end repeater for partners ?>


			
				
        <?php
				// Check if ACF repeater rows exists for files.
				if( have_rows('ucla-e-files') ):
				?>
				<div class="ucla-c-file-list">
				<h3>Flyer</h3>
				<ul>
					<?php 	
					// Loop through rows
					while( have_rows('ucla-e-files') ) : the_row(); ?>
					<li>
						<?php 
								// https://www.advancedcustomfields.com/resources/file/
								$file = get_sub_field('ucla-e-file'); 
								if( $file ): 
									// Extract values from file.
    							$url = $file['url'];
    							$title = $file['title'];
									$caption = $file['caption'];  // not displaying caption
									$description = "<span class=\"p-summary\">" . $file['description'] . "</span>"; // for displaying discription
								?>
								<a href="<?php echo esc_attr($url); ?>"><?php echo esc_attr($title); ?></a> 
								<?php echo $description; ?></span>

						<?php endif; ?>
					</li>
					<?php // End loop for files.
						endwhile; ?>
				</ul>
				</div>
				
				<?php endif; // end repeater for files ?>

        <?php if( get_field('ucla-e-contact-name') ): ?>
         <p><b>For more information, please contact</b>:</p> 
          <p class="event-contact-name"><?php echo esc_html( get_field( 'ucla-e-contact-name' ) ); ?>
          <?php if( get_field('ucla-e-contact-email') ): ?>
            <span class="event-contact-email"><?php echo esc_html( get_field( 'ucla-e-contact-email' ) ); ?></span>
            <?php endif; ?>
            <?php if( get_field('ucla-e-contact-tel') ): ?>
            <span class="event-content-tel"><?php echo esc_html( get_field( 'ucla-e-contact-tel' ) ); ?></span>
            <?php endif; ?>
          </p>
        <?php endif; ?> 

        <?php 
				// display event status
				if( get_field_object('ucla-e-status') ): 
				$event_status = get_field_object('ucla-e-status');
				$event_status_value = $event_status['value'];
				$event_status_label = $event_status['choices'][ $event_status_value ];
				?>
				<p><b>Event Status</b>: <?php echo esc_html( $event_status_label ); ?></p>
				<?php endif; ?>


        <?php if( get_field('ucla-e-price') ): ?>
        <p class="event-cost"><b>Price</b>: <?php echo esc_html( get_field( 'ucla-e-price' ) ); ?></p>
        <?php endif; ?>
        <?php if( get_field('ucla-e-audience') ): ?>
        <p class="event-cost"><b>Audience</b>: <?php echo esc_html( get_field( 'ucla-e-audience' ) ); ?></p>
        <?php endif; ?>
       

        <?php 
				// display event mode
				if( get_field_object('ucla-e-attendance-mode') ): 
				$event_attendence = get_field_object('ucla-e-attendance-mode');
				$event_attendence_value = $event_attendence['value'];
				$event_attendence_label = $event_attendence['choices'][ $event_attendence_value ];
				?>
				<p><b>Attendence mode</b>: <?php echo esc_html( $event_attendence_label ); ?></p>
				<?php endif; ?>

  </div>
		

		<?php
		get_template_part( 'template-parts/navigation' );

	}

	?>
      
</article>
