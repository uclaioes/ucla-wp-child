<?php
// Load the Parent and Child Stylesheets
add_action( 'wp_enqueue_scripts', 'ucla_theme_enqueue_styles' );
function ucla_theme_enqueue_styles() {
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array( 'ucla-style' ),
        wp_get_theme()->get('Version') // this only works if you have Version in the style header
    );
}


// DISABLE CUSTOMIZER CSS
add_action( 'customize_register', 'ucla_customize_register' );

function ucla_customize_register( $wp_customize ) {

  $wp_customize->remove_control( 'custom_css' );
}

function ucla_site_setup() {
// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

//add_action( 'after_setup_theme', 'twentytwenty_block_editor_settings', 10 );
//add_action( 'wp_enqueue_scripts', 'twentytwenty_register_styles' );

}

add_action( 'wp_enqueue_scripts', 'ucla_site_setup' );

// upgrade jquery to latest version.
function replace_core_jquery_version() {
	wp_deregister_script( 'jquery' );
	// Change the URL if you want to load a local copy of jQuery from your own server.
	wp_register_script( 'jquery', get_stylesheet_directory_uri() . '/assets/js/vendor/jquery-3.5.1.min.js' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

// Custom FavIcons
function ucla_favicon(){ ?>
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/favicon.ico"/>
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri();?>/assets/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri();?>/assets/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri();?>/assets/images/favicon-16x16.png">
<?php }
add_action('wp_head','ucla_favicon');


// Add custom image sizes
add_image_size( 'square_lrg', '800', '800', true );
add_image_size( 'square_thumb', '300', '300', true ); 

// Check if a page is a sub page.
// https://developer.wordpress.org/themes/basics/conditional-tags/
function is_subpage() {
	global $post;                              // load details about this page

	if ( is_page() && $post->post_parent ) {   // test to see if the page has a parent
			return $post->post_parent;             // return the ID of the parent post

	} else {                                   // there is no parent so ...
			return false;                          // ... the answer to the question is false
	}
}

// Check if a page is a parent or a child page.
// $pid = The ID of the page we're looking for pages underneath
function is_page_child($pid) { 
  global $post;         // load details about this page
  $anc = get_post_ancestors( $post->ID );
  foreach($anc as $ancestor) {
      if(is_page() && $ancestor == $pid) {
          return true;
      }
  }
  if(is_page()&&(is_page($pid)))
     return true;   // we're at the page or at a sub page
  else
      return false;  // we're elsewhere
};


// https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
add_filter( 'post_thumbnail_html', 'my_post_image_html', 10, 3 );
 
function my_post_image_html( $html, $post_id, $post_image_id ) {
 	$html = '<a href="' . get_permalink( $post_id ) . '">' . $html . '</a>';
	return $html;
}


/**
 * PRINT DATE FUNCTIONS
 */

function ucla_datetime_object( $field_name )
{
$date = new DateTime( $field_name );
echo $date->format('Ymd');
}	

function ucla_unixtimestamp( $field_name )
{
$date = new DateTime( $field_name );
echo $date->format('d.m.Y H:i:s');
}	



function ucla_html_datetime( $field_name )
{
$date = new DateTime( $field_name ) ;
echo $date->format('Y-m-d H:i');
}

function ucla_html_date( $field_name )
{
$date = new DateTime( $field_name ) ;
echo $date->format('Y-m-d');
}


function ucla_html_time( $field_name )
{
$date = new DateTime( $field_name ) ;
echo $date->format('H:i');
}

function ucla_year( $field_name )
{
$date = new DateTime( $field_name );
echo $date->format('Y');
}

function ucla_public_date( $field_name )
{
$date =  new DateTime( $field_name) ;
	echo $date -> format('F j, Y');
}

function ucla_public_datetime( $field_name )
{
$date =  new DateTime( $field_name) ;
	echo $date -> format('F j, Y, g:i a');
}

function ucla_public_time( $field_name )
{
$date =  new DateTime( $field_name) ;
	echo $date -> format('g:i a');
}

function ucla_public_date_format( $field_name, $format )
{
//$format = ('l, F j, Y, g:i a');	
$date =  new DateTime( $field_name);
	echo $date -> format($format);
}